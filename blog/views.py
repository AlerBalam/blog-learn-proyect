from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django.urls import reverse_lazy
from django.views import View
from .models import Post 
from .forms import PostForm 


# Generic views
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic import CreateView
from django.views.generic.edit import UpdateView

class PostViewMixin(View):
	model 		= Post
	form_class 	= PostForm
	

# def post_list(request):
# 	posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
# 	return render(request, 'blog/post_list.html', {'posts': posts})


class PostListView(PostViewMixin):
	template 	= 'blog/post_list.html'

	def get(self, request, *args, **kwargs):
		posts = self.model.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
		return render(request, self.template, {'posts': posts})

class PostListGView(ListView):
	model = Post
	template_name = 'blog/post_list.html'
	queryset = model.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
	context_object_name = 'posts'
	paginate_by = 3

	# def get_queryset = self.model.objects.filter(published_date__lte=timezone.now()).order_by('published_date')


# ////////////////////////////////////////////////////////


class PostEditView(PostViewMixin):
	template	= 'blog/post_edit.html'

	def get(self, request, pk, *args, **kwargs):
		post = get_object_or_404(self.model, pk=pk)
		form = self.form_class(instance=post)
		return render(request, self.template, {'form': form})

	def post(self, request, pk, *args, **kwargs):
		post = get_object_or_404(self.model, pk=pk)
		form = self.form_class(request.POST, instance=post)
		if form.is_valid():
			post = form.save(commit=False)
			post.author = request.user
			post.published_date = timezone.now()
			post.save()
			return redirect('post_detail', pk=post.pk)			
		return render(request, self.template, {'form': form})


class PostEditGView(UpdateView):
	model = Post
	form = PostForm
	fields = ['title', 'text']
	template_name = 'blog/post_edit.html'
	# success_url = reverse_lazy('post_detail')

	def form_valid(self, form):
		post = form.save(commit=False)
		post.author = self.request.user
		post.published_date = timezone.now()
		post.save()
		return redirect('post_detail', pk=form.instance.pk)
		# return redirect('post_detail', pk=self.kwargs['pk'])


# /////////////////////////////////////////////////////////////


class PostNewView(PostViewMixin):
	template = 'blog/post_new.html'

	def get(self, request, *args, **kwargs):
		form = self.form_class()
		return render(request, self.template, {'form': form})		

	def post(self, request, *args, **kwargs):
		form = self.form_class(request.POST)
		if form.is_valid():
			post = form.save(commit=False)
			post.author = request.user
			post.published_date = timezone.now()
			post.save()
			return redirect('post_detail', pk=post.pk)				
		return redirect('post_list')			


class PostNewGView(CreateView):
	model = Post
	form_class = PostForm
	template_name = 'blog/post_new.html'
	success_url = 'post_detail'


	def form_valid(self, form):
		post = form.save(commit=False)
		post.author = self.request.user
		post.published_date = timezone.now()
		post.save()
		return redirect('post_detail', pk=post.pk)



# ///////////////////////////////////////////////////////////////


class PostDetailView(PostNewView):
	template = 'blog/post_detail.html'

	def get(self, request, pk, *args, **kwargs):
		post = get_object_or_404(Post, pk=pk)
		return render(request, self.template, {'post': post})
	

class PostDetailGView(DetailView):
	model = Post
	template_name = 'blog/post_detail.html'
	context_object_name = 'post'
	# queryset = model.objects.all()


# ///////////////////////////////////////////////////////////////


	# def get_object(self):
	# 	course = get_object_or_404(self.model, pk=self.kwargs['pk'])
	# 	return self.model.objects.filter(published_date__lte=timezone.now()).order_by('published_date')

# def post_detail(request, pk):
# 	post = get_object_or_404(Post, pk=pk)
# 	return render(request, 'blog/post_detail.html', {'post': post})

# def post_new(request):
# 	if request.method == "POST":
# 		form = PostForm(request.POST)
# 		if form.is_valid():
# 			post = form.save(commit=False)
# 			post.author = request.user
# 			post.published_date = timezone.now()
# 			post.save()
# 			return redirect('post_detail', pk=post.pk)
# 	else:
# 		form = PostForm()
# 	return render(request, 'blog/post_new.html', {'form': form})

# def post_edit(request, pk):
#     post = get_object_or_404(Post, pk=pk)
#     if request.method == "POST":
#         form = PostForm(request.POST, instance=post)
#         if form.is_valid():
#             post = form.save(commit=False)
#             post.author = request.user
#             post.published_date = timezone.now()
#             post.save()
#             return redirect('post_detail', pk=post.pk)
#     else:
#         form = PostForm(instance=post)
#     return render(request, 'blog/post_edit.html', {'form': form})



