from django import forms
from .models import Post

class PostForm(forms.ModelForm):
	class Meta:
		model = Post
		fields = ('title', 'text')



# class LoginForm(forms.Form):
# 	email = forms.EmailField(max_length=50)
# 	password = forms.CharField(null=True)
