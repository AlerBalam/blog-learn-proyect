from django.urls import path
from . import views

urlpatterns = [
    path('', views.PostListGView.as_view(), name='post_list'),
    path('post/new/', views.PostNewGView.as_view(), name='post_new'),
    path('post/<int:pk>/', views.PostDetailGView.as_view(), name='post_detail'),
    path('post/<int:pk>/edit/', views.PostEditGView.as_view(), name='post_edit'),
]