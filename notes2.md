DJango

path() - route, view              kwargs (argumentos arbitrarios de palabra clave) y name

import sound.effects.echo
from sound.effects import echo
from sound.effects.echo import echofilter
from package import * => definir variable __all__ en __init__


Configurar wsgi con apache ***
https://docs.djangoproject.com/es/2.1/howto/deployment/wsgi/modwsgi/

Configurar Settings ***
https://docs.djangoproject.com/es/2.1/ref/settings/
https://docs.djangoproject.com/es/2.1/topics/settings/

// Ver cambios en los settings del proyecto
python manage.py diffsettings

Entender Cache ***
https://docs.djangoproject.com/es/2.1/topics/cache/
https://docs.djangoproject.com/es/2.1/topics/cache/#the-per-site-cache


URL
Modelos
ORM
Vistas (FUNCTION VIEWS/CLASS BASED VIEWS)
FORMS (MODEL FORMS)
TEMPLATES

CLASS BASED VIEWS
GENERIC VIEWS ///

REST API
django rest framework
routers, views y serializers


GENERIC API VIEWS - en lugar de ViewSets
filters


DJANGO AUTH



render

ModelForm



/// PERFORMANCE make Django Faster - Good Practices
https://docs.djangoproject.com/en/2.1/topics/performance/