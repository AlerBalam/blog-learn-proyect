from rest_framework import routers
from . import views

router = routers.SimpleRouter()
router.register(r's', views.SeriesViewSet)
# router.register(r'accounts', AccountViewSet)

urlpatterns = router.urls





# from django.conf.urls import url
# 	# from django.urls import path, include
# 	# from rest_framework import routers
# from . import views

# router = routers.DefaultRouter()
# router.register('', views.SeriesViewSet.as_view({'get': 'list'}))


# urlpatterns = [
# 	# path('s/', include(router.urls)),
# 	url(r'^s/$', views.SeriesViewSet.as_view({'get': 'list'})),
# 	url(r'^s/(?P<pk>[0-9]+)/$', views.SeriesViewSet.as_view({'get': 'retrieve'})),
# 	# url(r'^s/$', views.serie_list),
# 	# url(r'^s/(?P<pk>[0-9]+)/$', views.serie_detail),
# ]
